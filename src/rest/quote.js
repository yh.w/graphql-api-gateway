// src/rest/quote.js
import app from 'express';
const router = app.Router();
import rootSchema from '../app.js';
import { graphql } from 'graphql'
const query = (q, vars) => {
    return graphql({ schema: rootSchema, source: q });
}
// Transform response to JSON API format
const transform = (result) => {
    const quote = result.data.quote;
    return {
        data: {
            type: 'quote',
            id: quote.id,
            attributes: {
                quote: quote.quote,
                author: quote.author,
                timestamp: quote.timestamp,
            }
        }
    }
}
// REST request to get a quote
router.get('/', (req, res) => {
    // Convert the request into a GraphQL query string
    query("query{quote{id, quote, author, timestamp}}")
        .then(result => {
            const transformed = transform(result)
            res.send(transformed)
        })
        .catch(err => {
            res.sendStatus(500)
        })
})
export default router;