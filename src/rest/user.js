import app from 'express';
const router = app.Router();
import rootSchema from '../app.js';
import { graphql } from 'graphql'
const query = (q, vars) => {
    return graphql({ schema: rootSchema, source: q });
}
// Transform response to JSON API format
// (if desired)
const transform = (result, id) => {
    const user = result.data.users[0];
    return {
        data: {
            type: 'user',
            id: user.id,
            attributes: {
                name: user.name,
                status: user.status,
            }
        }
    }
}
// REST request to get a user
router.get('/:userId', (req, res) => {
    // Convert the request into a GraphQL query string
    query(`query{users(id:"${req.params.userId}"){id, name, status}}`)
        .then(result => {
            const transformed = transform(result, req.params.userId)
            res.send(transformed)
        })
        .catch(err => {
            res.sendStatus(500)
        })
})
export default router;