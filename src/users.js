import {
    GraphQLList,
    GraphQLObjectType,
    GraphQLString
} from 'graphql';
import { getUser } from './services/user.js'
export const UserType = new GraphQLObjectType({
    name: 'User',
    description: 'User',
    fields: () => ({
        id: {
            type: GraphQLString,
            description: 'User id',
        },
        name: {
            type: GraphQLString,
            description: 'User name',
        },
        status: {
            type: GraphQLString,
            description: 'Status',
        }
    })
});
export default {
    type: new GraphQLList(UserType),
    args: {
      id: { type: GraphQLString },
    },
    resolve: getUser
}