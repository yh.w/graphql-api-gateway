// src/services/quote.js
/**
 * This is where the app calls the microservice responsible for the "Quote" resource type.
 */
export const
    getQuote = () => {
        return {
            id: Math.floor(Math.random() * 999999),
            quote: `#${Math.floor(Math.random() * 999999)}`,
            author: 'author',
            timestamp: new Date().toUTCString(),
        };
    }