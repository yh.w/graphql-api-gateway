import {
    GraphQLObjectType,
    GraphQLNonNull,
    GraphQLString
} from 'graphql';
import { getQuote } from './services/quote.js'
export const QuoteType = new GraphQLObjectType({
    name: 'Quote',
    description: 'Quote of the day from API service',
    fields: () => ({
        id: {
            type: GraphQLString,
            description: 'Quote id',
        },
        quote: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'The text of the quote',
        },
        author: {
            type: GraphQLString,
            description: 'The person to whom the quote is attributed',
        },
        timestamp: {
            type: GraphQLString,
            description: 'Timestamp',
        }
    })
});
export default {
    type: QuoteType,
    resolve: getQuote
}