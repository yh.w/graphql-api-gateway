// src/app.js
import {
    GraphQLObjectType,
    GraphQLSchema,
} from 'graphql';
import userQuery from './users.js';
import quoteQuery from './quote.js';
const query = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        users: userQuery,
        quote: quoteQuery,
    },
});
export default new GraphQLSchema({
    query,
});