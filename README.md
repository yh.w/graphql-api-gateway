# GraphQL API Gateway

## Scripts

```
yarn
yarn run start
```

## Host

`localhost:3000`

## Features

 - Serving both GraphQL & REST
 - Translating a REST request into its GraphQL equivalent -> `/users/:id`
 - Requesting a microservice -> `/quote`

ref: https://www.cloudbees.com/blog/graphql-as-an-api-gateway-to-micro-services
