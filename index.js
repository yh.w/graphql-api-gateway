import express from 'express';
const app = express();
import users from './src/rest/user.js';
import quotes from './src/rest/quote.js';

app.use('/users', users);
app.use('/quote', quotes);

app.listen(3000);